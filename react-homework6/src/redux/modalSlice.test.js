import modalReducer, { openModal, closeModal } from './modalSlice';

describe('modalReducer', () => {
  const initialState = {
    isOpen: false,
    modalType: null,
    modalId: null,
  };

  it('should handle openModal', () => {
    const action = openModal({ modalType: 'example', id: 1 });
    const nextState = modalReducer(initialState, action);
    expect(nextState.isOpen).toEqual(true);
    expect(nextState.modalType).toEqual('example');
    expect(nextState.modalId).toEqual(1);
  });

  it('should handle closeModal', () => {
    const action = closeModal();
    const state = { ...initialState, isOpen: true, modalType: 'example', modalId: 1 };
    const nextState = modalReducer(state, action);
    expect(nextState.isOpen).toEqual(false);
    expect(nextState.modalType).toEqual(null);
    expect(nextState.modalId).toEqual(null);
  });
});