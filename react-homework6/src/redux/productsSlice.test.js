import productsReducer, { addToCart, decrementCartItem, incrementCartItem, removeFromCart } from './productsSlice';

describe('productsReducer', () => {
  const initialState = {
    items: [],
    status: 'idle',
    error: null,
    cart: [],
  };

  it('should handle addToCart', () => {
    const action = addToCart({ id: 1 });
    const nextState = productsReducer(initialState, action);
    expect(nextState.cart).toEqual([{ id: 1, quantity: 1 }]);
  });

  it('should handle decrementCartItem', () => {
    const action = decrementCartItem({ id: 1 });
    const state = { ...initialState, cart: [{ id: 1, quantity: 2 }] };
    const nextState = productsReducer(state, action);
    expect(nextState.cart).toEqual([{ id: 1, quantity: 1 }]);
  });

  it('should handle incrementCartItem', () => {
    const action = incrementCartItem({ id: 1 });
    const state = { ...initialState, cart: [{ id: 1, quantity: 2 }] };
    const nextState = productsReducer(state, action);
    expect(nextState.cart).toEqual([{ id: 1, quantity: 3 }]);
  });

  it('should handle removeFromCart', () => {
    const action = removeFromCart({ id: 1 });
    const state = { ...initialState, cart: [{ id: 1, quantity: 2 }] };
    const nextState = productsReducer(state, action);
    expect(nextState.cart).toEqual([]);
  });
});