import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Modal from './Modal';

describe('Modal Component', () => {
  const defaultProps = {
    handleCloseModal: jest.fn(),
    closeButton: true,
    showImage: true,
    header: 'Test Header',
    text: 'Test text',
    image: 'test-image-url',
    actions: <button>Test Action</button>
  };

  const renderModal = (props = {}) => {
    return render(<Modal {...defaultProps} {...props} />);
  };

  test('renders the modal with correct content', () => {
    renderModal();

    expect(screen.getByText('Test Header')).toBeInTheDocument();
    expect(screen.getByText('Test text')).toBeInTheDocument();
    expect(screen.getByAltText('Modal Image')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Test Action' })).toBeInTheDocument();
  });

  test('calls handleCloseModal when clicking outside the modal content', () => {
    renderModal();

    fireEvent.click(screen.getByRole('button', { name: 'Test Action' }));

    expect(defaultProps.handleCloseModal).not.toHaveBeenCalled();

    fireEvent.click(screen.getByText('Test text').parentElement.parentElement.parentElement); 

    expect(defaultProps.handleCloseModal).toHaveBeenCalled();
  });

  test('calls handleCloseModal when clicking on close button', () => {
    renderModal();

    fireEvent.click(screen.getByText('×'));

    expect(defaultProps.handleCloseModal).toHaveBeenCalled();
  });

  test('does not show image if showImage is false', () => {
    renderModal({ showImage: false });

    expect(screen.queryByAltText('Modal Image')).not.toBeInTheDocument();
  });

  test('does not show close button if closeButton is false', () => {
    renderModal({ closeButton: false });

    expect(screen.queryByText('×')).not.toBeInTheDocument();
  });
});