import React from 'react';
import renderer from 'react-test-renderer';
import Modal from './Modal';

describe('Modal Component Snapshot', () => {
  const defaultProps = {
    handleCloseModal: jest.fn(),
    closeButton: true,
    showImage: true,
    header: 'Test Header',
    text: 'Test text',
    image: 'test-image-url',
    actions: <button>Test Action</button>
  };

  test('matches the snapshot', () => {
    const tree = renderer.create(<Modal {...defaultProps} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});