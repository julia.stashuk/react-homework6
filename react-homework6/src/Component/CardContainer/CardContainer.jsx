import PropTypes from 'prop-types';
import React, { useContext } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import "./CardContainer.scss";
import { ViewContext } from '../../ContextApi/ViewContext';
import ProductTable from '../../ContextApi/ProductTable';

const CardContainer = ({ products = [], handleAddToCart, handleRemoveFromCart, handleFavourite = () => { } }) => {
    const { view } = useContext(ViewContext);

    return (
        <div className={`card-container ${view === 'grid' ? 'grid-view' : 'table-view'}`}>
            {view === 'grid' ? (
            products.map((product) => {
            const { title, price, id, color, image, isFavourite, isAddedToCart } = product
                
               return (
               <ProductCard key={id} title={title} price={price}
                    id={id} color={color} image={image} handleRemoveFromCart={handleRemoveFromCart}
                    isFavourite={isFavourite} handleFavourite={handleFavourite} 
                    handleAddToCart={handleAddToCart} isAddedToCart={isAddedToCart}
                    product={product}/>
               );
            })
        ) : (
            <ProductTable
              products={products}
              handleAddToCart={handleAddToCart}
              handleFavourite={handleFavourite}
            />
          )} 
    </div>
    );
};

CardContainer.propTypes = {
    products: PropTypes.array,
    handleFavourite:PropTypes.func,
    handleRemoveFromCart: PropTypes.func
};

export default CardContainer;
