import React from 'react';
import renderer from 'react-test-renderer';
import Button from './Button';

describe('Button onClick Test', () => {
  const defaultProps = {
    type: 'button',
    className: 'test-button',
    text: 'Test Button',
    onClick: jest.fn(),
  };

  test('calls onClick prop when clicked', () => {
    const button = renderer.create(<Button {...defaultProps} />).root.findByType('button');
    button.props.onClick();
    expect(defaultProps.onClick).toHaveBeenCalledTimes(1);
  });
});