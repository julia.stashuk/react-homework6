import React from 'react';
import renderer from 'react-test-renderer';
import Button from './Button';

describe('Button Component Snapshot', () => {
  const defaultProps = {
    type: 'button',
    className: 'test-button',
    text: 'Test Button',
    onClick: jest.fn(),
  };

  test('matches the snapshot', () => {
    const tree = renderer.create(<Button {...defaultProps} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});