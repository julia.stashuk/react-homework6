import React, { useEffect} from 'react';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts, addToCart, incrementCartItem, decrementCartItem, removeFromCart } from './redux/productsSlice';
import Menu from './Component/Menu/Menu';
import AppRouter from './AppRouter';
import { ViewProvider } from './ContextApi/ViewContext';

function App() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.items);
  const status = useSelector((state) => state.products.status);
  const error = useSelector((state) => state.products.error);
  const cart = useSelector((state) => state.products.cart);

  useEffect(() => {
    if (status === 'idle') {
      dispatch(fetchProducts());
    }
  }, [status, dispatch]);

  const [favoriteProducts, setFavoriteProducts] = React.useState(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    return favorites;
  });

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favoriteProducts));
  }, [favoriteProducts]);


  const handleFavourite = (id) => {
    setFavoriteProducts((prevFavorites) => {
      const newFavorites = prevFavorites.includes(id)
        ? prevFavorites.filter((favId) => favId !== id)
        : [...prevFavorites, id];
      localStorage.setItem('favorites', JSON.stringify(newFavorites));
      return newFavorites;
    });
  };

  const handleAddToCart = (id) => {
    dispatch(addToCart({ id }));
  };

  const handleDecrementCartItem = (id) => {
    dispatch(decrementCartItem({ id }));
  };

  const handleIncrementCartItem = (id) => {
    dispatch(incrementCartItem({ id }));
  };

  const handleRemoveFromCart = (id) => {
    dispatch(removeFromCart({ id }));
  };

  return (
    <ViewProvider>
    <div>
      <Menu favoriteProducts={favoriteProducts} cart={cart} handleAddToCart={handleAddToCart}/>
      <AppRouter
        products={products}
        handleFavourite={handleFavourite}
        cart={cart}
        handleRemoveFromCart={handleRemoveFromCart}
        handleAddToCart={handleAddToCart}
        decrementCartItem={handleDecrementCartItem}
        incrementCartItem={handleIncrementCartItem}

      />
    </div>
    </ViewProvider>
  );
}

export default App;

