import CardContainer from "../../Component/CardContainer/CardContainer"
import ViewSwitcher from "../../ContextApi/ViewSwitcher"; 

const Home = ({ products, handleFavourite, handleAddToCart, handleRemoveFromCart }) => {
    return (
      <div>
        <ViewSwitcher />
        <CardContainer 
          products={products} 
          handleFavourite={handleFavourite} 
          handleAddToCart={handleAddToCart}
          handleRemoveFromCart={handleRemoveFromCart}
        />
      </div>
    );
  }

export default Home