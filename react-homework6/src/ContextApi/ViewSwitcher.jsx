import React, { useContext } from 'react';
import { ViewContext } from './ViewContext';

const ViewSwitcher = () => {
  const { view, setView } = useContext(ViewContext);

  return (
    <div>
      <button onClick={() => setView('grid')} disabled={view === 'grid'}>Card View</button>
      <button onClick={() => setView('table')} disabled={view === 'table'}>Table View</button>
    </div>
  );
};

export default ViewSwitcher;