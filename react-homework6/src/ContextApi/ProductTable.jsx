import React, { useState, useEffect } from 'react';
import "./ProductTable.scss";
import Button from '../Component/Button/Button';
import SecondModal from '../Component/SecondModal/SecondModal';
import { useDispatch, useSelector } from 'react-redux';
import { openModal, closeModal } from '../redux/modalSlice';
import { addToCart } from '../redux/productsSlice';

const ProductTable = ({ 
  products = [],
  handleFavourite, 
}) => {
  const dispatch = useDispatch();
  const isModalOpen = useSelector((state) => state.modal.isOpen);
  const modalId = useSelector((state) => state.modal.modalId);
  const cart = useSelector((state) => state.products.cart);

  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const storedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavorites(storedFavorites);
  }, []);

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  const handleStarClick = (id) => {
    handleFavourite(id);
    if (favorites.includes(id)) {
      setFavorites(favorites.filter(favId => favId !== id));
    } else {
      setFavorites([...favorites, id]);
    }
  };

  const handleAddToCartClick = (id) => {
    dispatch(addToCart({ id }));
    dispatch(closeModal());
  };

  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Color</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {products.map(product => (
          <tr key={product.id}>
            <td>{product.title}</td>
            <td>{product.price}</td>
            <td>{product.color}</td>
            <td>
            <Button 
                className={`main-button favorite-button ${favorites.includes(product.id) ? 'active' : ''}`}
                onClick={() => handleStarClick(product.id)}
                text="Favorite"
              />
              <Button
                className={`main-button ${cart.some(item => item.id === product.id) ? 'added-to-cart' : ''}`}
                onClick={() => {
                  if (!cart.some(item => item.id === product.id) && !isModalOpen) {
                    dispatch(openModal({ modalType: "second", id: product.id }));
                  }
                }}
                text={cart.some(item => item.id === product.id) ? "Added to Cart" : "Add to Cart"}
              />
              {isModalOpen && modalId === product.id && (
                <SecondModal
                  handleAddToCart={() => handleAddToCartClick(product.id)}
                  handleCloseModal={() => dispatch(closeModal())}
                  title={product.title}
                  price={product.price}
                  id={product.id}
                />
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default ProductTable;